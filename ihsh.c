/***************************************************************************//**
  @file         ihsh.c
  @author       Jacob Hanko and Izaak Miller
  @date         26 April 2017
  @brief        IHSH (Izaak-Hanko SHell)

  Borrowed code from Lab 2...
  which borrowed code from a tutorial written
  by Stephen Brennanc

  Left all 3 of Stephen's methods
  Left our Lab 2 added Square method
  Added:


  To run, gcc ihsh.c (optional -o ihsh)
  ./a.out
  (if -o flag used) ./ihsh
*******************************************************************************/

#include <sys/wait.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>


/*
  Function Declarations for builtin shell commands:
 */
int ihsh_cd(char **args);
int ihsh_help(char **args);
int ihsh_exit(char **args);
int ihsh_quit(char **args);
int ihsh_square(char **args);
int ihsh_fact(char **args);
int ihsh_break(char **args);
//int ihsh_history(char **hist[], int current);
//figure out how to deal with this^^^ cannot print char **.
//When calling the function in the builtin_func it is an char** instead
//of char*. Might have to create something separate in order to handle this

/*
  List of builtin commands, followed by their corresponding functions.
 */
char *builtin_str[] = {
  "cd",
  "help",
  "exit",
  "quit",
  "square",
  "fact",
  "break"
};

int (*builtin_func[]) (char **) = {
  &ihsh_cd,
  &ihsh_help,
  &ihsh_exit,
  &ihsh_quit,
  &ihsh_square,
  &ihsh_fact,
  &ihsh_break
};

int ihsh_num_builtins() {
  return sizeof(builtin_str) / sizeof(char *);
}

/*
  Builtin function implementations.
*/

/**
   Square function added by Jacob Hanko
   Returns a 7 x 7 square
*/

int ihsh_square(char **args)
{
  printf("The Square command.\n");
  printf("Drawing a 7 by 7 square\n");

  //Prints the actual 7 by 7 square
  int i;
  for (i = 0; i < 7; i++) {
    printf("x x x x x x x\n");
  }

  printf("Square command is complete!\n");

  return 1;
}

/*
* Ouputs a fun tough twister for our users
*/
int ihsh_fact(char **args)
{
  printf("She sells sea shells by the sea shore.\n");

return 1;
}

/*
* Breaks the code
*/
int ihsh_break(char **args)
{
  while(1)
  {

  }

}

/**
   @brief Bultin command: change directory.
   @param args List of args.  args[0] is "cd".  args[1] is the directory.
   @return Always returns 1, to continue executing.
 */
int ihsh_cd(char **args)
{
  if (args[1] == NULL) {
    fprintf(stderr, "ihsh: expected argument to \"cd\"\n");
  } else {
    if (chdir(args[1]) != 0) {
      perror("ihsh");
    }
  }
  return 1;
}

/**
   @brief Builtin command: print help.
   @param args List of args.  Not examined.
   @return Always returns 1, to continue executing.
 */
int ihsh_help(char **args)
{
  int i;
  printf("IHSH written for CS 440 Final Project\n");
  printf("Written by Jacob Hanko and Izaak Miller\n");
  printf("Type program names and arguments, and hit enter.\n");
  printf("The following are built in:\n");

  for (i = 0; i < ihsh_num_builtins(); i++) {
    printf("  %s\n", builtin_str[i]);
  }

  printf("Use the man command for information on other programs.\n");
  ihsh_exit(args);
  return 1;
}

/**
   @brief Builtin command: exit.
   @param args List of args.  Not examined.
   @return Always returns 0, to terminate execution.
 */
int ihsh_exit(char **args)
{
  return 0;
}

 // built in command: quit.
 // another way to terminate the execution

int ihsh_quit(char **args)
{
  return 0;
}

// a history command that will print out that previous 20 commands

#define HISTORY_COUNT 20
int ihsh_history(char *hist[], int current)
{
  int i = current;
  int hist_num = 1;

  do {
      if (hist[i]) {
        printf("%4d  %s\n", hist_num, hist[i]); //printing out the last commands
        hist_num++;
          }
          i = (i + 1) % HISTORY_COUNT;

  } while (i != current);

  return 1;
}

// a clear history command that will remove that last 20 saved commands
int ihsh_clear_history(char *hist[])
{
        int i;
        for (i = 0; i < HISTORY_COUNT; i++) {
                free(hist[i]);
                hist[i] = NULL;
        }

        return 1;
}

/**
  @brief Launch a program and wait for it to terminate.
  @param args Null terminated list of arguments (including program).
  @return Always returns 1, to continue execution.
 */
int ihsh_launch(char **args)
{
  pid_t pid;
  int status;

  pid = fork();
  if (pid == 0) {
    // Child process
    if (execvp(args[0], args) == -1) {
      perror("ihsh");
    }
    exit(EXIT_FAILURE);
  } else if (pid < 0) {
    // Error forking
    perror("ihsh");
  } else {
    // Parent process
    do {
      waitpid(pid, &status, WUNTRACED);
    } while (!WIFEXITED(status) && !WIFSIGNALED(status));
  }

  return 1;
}


/**
   @brief Execute shell built-in or launch program.
   @param args Null terminated list of arguments.
   @return 1 if the shell should continue running, 0 if it should terminate
 */
int ihsh_execute(char **args, char *hist[], int current)
{
  int j;

  if (args[0] == NULL) {
    // An empty command was entered.
    return 1;
  }



// checks argument to see what it is
// then launches corresponding command based on command

  for (j = 0; j < ihsh_num_builtins(); j++) {
    if (strcmp(args[0], builtin_str[j]) == 0) {
      return (*builtin_func[j])(args);
    } else if (strcmp(args[0], "history") == 0) {
        return(ihsh_history(hist, current));
    } else if (strcmp(args[0], "clear_history") == 0){
        return(ihsh_clear_history(hist));
    } else if (strcmp(args[0], "!") == 0){
      //run last used command
      args[0] = hist[current-1];
      j = j - 1;
    }
  }

  return ihsh_launch(args);
}

#define ihsh_RL_BUFSIZE 1024
/**
   @brief Read a line of input from stdin.
   @return The line from stdin.
 */
char *ihsh_read_line(void)
{
  int bufsize = ihsh_RL_BUFSIZE;
  int position = 0;
  char *buffer = malloc(sizeof(char) * bufsize);
  int c;

  if (!buffer) {
    fprintf(stderr, "ihsh: allocation error\n");
    exit(EXIT_FAILURE);
  }

  while (1) {
    // Read a character
    c = getchar();

    // If we hit EOF, replace it with a null character and return.
    if (c == EOF || c == '\n') {
      buffer[position] = '\0';
      return buffer;
    } else {
      buffer[position] = c;
    }
    position++;

    // If we have exceeded the buffer, reallocate.
    if (position >= bufsize) {
      bufsize += ihsh_RL_BUFSIZE;
      buffer = realloc(buffer, bufsize);
      if (!buffer) {
        fprintf(stderr, "ihsh: allocation error\n");
        exit(EXIT_FAILURE);
      }
    }
  }
}

#define ihsh_TOK_BUFSIZE 64
#define ihsh_TOK_DELIM " \t\r\n\a"
/**
   @brief Split a line into tokens (very naively).
   @param line The line.
   @return Null-terminated array of tokens.
 */
char **ihsh_split_line(char *line)
{
  int bufsize = ihsh_TOK_BUFSIZE, position = 0;
  char **tokens = malloc(bufsize * sizeof(char*));
  char *token, **tokens_backup;

  if (!tokens) {
    fprintf(stderr, "ihsh: allocation error\n");
    exit(EXIT_FAILURE);
  }

  token = strtok(line, ihsh_TOK_DELIM);
  while (token != NULL) {
    tokens[position] = token;
    position++;

    if (position >= bufsize) {
      bufsize += ihsh_TOK_BUFSIZE;
      tokens_backup = tokens;
      tokens = realloc(tokens, bufsize * sizeof(char*));
      if (!tokens) {
		free(tokens_backup);
        fprintf(stderr, "ihsh: allocation error\n");
        exit(EXIT_FAILURE);
      }
    }

    token = strtok(NULL, ihsh_TOK_DELIM);
  }
  tokens[position] = NULL;
  return tokens;
}

/**
   @brief Loop getting input and executing it.
 */
void ihsh_loop(void)
{
  char *line;
  char **args;
  int status;
  int i, current = 0;
  char *hist[HISTORY_COUNT];
  char *prompt;
  long size;
  char *buf;

    //creates an empty list of 20 for the history
  for (i = 0; i < HISTORY_COUNT; i++){
    hist[i] = NULL;
  }
  do {
    //added a prompt
  size = pathconf(".", _PC_PATH_MAX);
  if ((buf = (char *)malloc((size_t)size)) != NULL){
    prompt = getcwd(buf, (size_t)size);
  }
  printf("ihsh@%s$ ", prompt);


  line = ihsh_read_line();
  args = ihsh_split_line(line);
  free(hist[current]);
  //saves command into history array
  hist[current] = strdup(args[0]);
  status = ihsh_execute(args, hist, current);


  free(line);
  free(args);
  current = (current + 1) % HISTORY_COUNT;

  } while (status);
}

/**
   @brief Main entry point.
   @param argc Argument count.
   @param argv Argument vector.
   @return status code
 */
int main(int argc, char **argv)
{
  // Load config files, if any.

  // Run command loop.
  ihsh_loop();

  // Perform any shutdown/cleanup.

  return EXIT_SUCCESS;
}
